#!/bin/bash
curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-6.5.3-x86_64.rpm
rpm -vi metricbeat-6.5.3-x86_64.rpm
cp /home/raviteja/metricbeat.yml /etc/metricbeat/metricbeat.yml
metricbeat modules enable system
metricbeat setup
systemctl restart metricbeat.service
systemctl enable metricbeat.service
systemctl status metricbeat.service
